# Python Übung 2: Signalerzeugung

## Aufgaben F-Level: 

<a name="F1"></a>

1. Plotten Sie 3 subplots untereinander. Der erste subplot soll eine Sinusfunktion (mit richtigem Zeitvektor auf der x-Achse) mit der Grundfrequenz 200 Hz, einer Samplingrate von 16000 Hz, einer Länge von 10 ms und einer Amplitude von 2 darstellen. Der zweite subplot soll eine Dreiecksfunktion und der dritte subplot eine Rechtecksfunktion jeweils mit den gleichen Parametern darstellen. [Tipps für F1](#C1)

<a name="F2"></a>

2. Schreiben Sie eine Funktion, die als Übergabeparameter, die Grundfrequenz, die Samplingrate, die Länge in Sekunden und eine List (oder numpy Array) mit den Amplituden (negative Werte sind zugelassen) der harmonischen Anteile bekommt. Bsp eine Übergabe von (100,16000,0.1, [1,-0.25,0,0.4]), sollte ergeben
$`x(k) = 1\cdot \sin(2\cdot \pi \cdot k \cdot 100/16000) -0.25\cdot \sin(2\cdot \pi \cdot k \cdot 2 \cdot 100/16000) + 0.4\cdot \sin(2\cdot \pi \cdot k \cdot 4 \cdot 100/16000)`$ mit 1600 Werten. Zeichnen Sie das Ergebnis für die Fourier-Reihe der ersten 7 Harmonischen eines Rechteck-Signals. [Tipps für F2](#C2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo oder Google), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow (https://stackoverflow.com/) oder zur Python-Referenz (https://docs.python.org/3/reference/). Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Ein digitaler Sinus ist definiert als $`x(kk) = A \cdot \sin(2\cdot \pi \cdot kk \cdot f_0/f_s)`$ mit der Amplitude $`A`$, der Grundfrequenz $`f_0`$, der Abtastrate $`f_s`$ und der eigentlichen Laufvaribale $`kk`$, die eine Zeitfolge der ganzzahligen Abtastwerte beinhaltet (z.B. $`kk =`$ [0 1 2 3 4 ... ]). Starten Sie mit der Erzeugung dieses Vektors $`kk = ...`$.  [Tipps für C11](#L11)

2. Ein digitales Dreickeckssignal kann abschnittsweise definiert werden. Eine Periode der Dreieckschwingung ist gegeben durch $`x(k) = A \cdot (1 - |2 \cdot k/L|)`$ für $`-L \leq k < L`$ mit der Amplitude $`A`$, der eigentlichen Laufvaribale $`k`$ und der Periodendauer $`P = 2\cdot L`$. Hierbei gibt es also stets eine aufsteigende Rampe bis $`k = 0`$, gefolgt von einer absteigenden Rampe. Suchen Sie nach einer Funktion, die ein numpy array wiederholt. [Tipps für C12](#L12)

3. Ein digitales Rechteckssignal kann durch das Vorzeichen (sign) der Sinusfunktion angegeben werden: $`x(kk) = A \cdot \text{sign}(\sin(2\cdot \pi \cdot kk \cdot f_0/f_s))`$. Es könnte also Sinn machen, die Erzeugung der Sinusfunktion aus der ersten Teilaufgabe in eine Funktion auszulagern `def get_sinus(f0, sample_rate, len_seconds, amplitude = 1):`. Für die Implementierung von `sign` gibt es viele Lösungen. Damit die Null als +A gesetzt wird, können Sie `np.sign` eigentlich nicht nutzen (Als ersten Test schon). Suchen Sie nach Indexierung mit logischen Operatoren als Alternative und bauen sich eine eigene sign Funktion. [Tipps für C13](#L13)

4. Nutzen Sie die Lösung der letzten Übung, um die drei Funktionen zu zeichnen. Keine Tipps notwendig.

[zurück zu F1](#F1)

<a name="C2">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 2:
</a>

1. Schreiben Sie (falls nicht bei Aufgabe 1 erledigt) zunächst ein Funktion für die Sinusfunktion und deren Parametern aus der ersten Aufgabe und testen diese. [Tipps für C21](#L21)
2. Erweitern Sie diese Funktion so, dass Sie testen, ob der Übergabeparameter für die Amplitude ein einzelner Wert oder ein Vektor ist. Wenn Sie den nächsten Teil gut programmieren, brauchen Sie diese Abfrage nicht. [Tipps für C22](#L22)
3. Nutzen Sie eine Schleife um über die Anzahl der Amplitudenwerte zu iterieren und die notwendigen Harmonische zur Grundfrequenz zu erzeugen
[Tipps für C23](#L23)
4. Suchen Sie die Fourier-Reihe für Rechteckfunktionen (zB aus dem Skript) [Tipps für C24](#L24)
5. Plotten Sie das erzeugte Signal und überlegen sich ob das Signal wie eine Approximation eines Rechteck aussieht. Überprüfen Sie dies, indem Sie den Versuch mit 15 Harmonischen wiederholen.[Tipps für C25](#L25)

[zurück zu F2](#F2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:
<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

1. Definieren Sie zunächst Ihre Parameter (`f0, sample_rate, amplitude, len_s`), also z.B. `f0 = 200` . 
2. Berechnen Sie die notwendige Anzahl an Samples `len_samples = ...`. (Siehe letzte Übung)
3. Erzeugen Sie den Zeitvektor `k = `. Nutzen Sie hierzu `np.arange` https://numpy.org/doc/stable/reference/generated/numpy.arange.html 
4. Erzeugen Sie nun `x = ` mit der im C Teil angegeben Formel. Achtung, den Index (k) geben Sie natürlich nicht an.
5. Für ein Erfolgserlebnis plotten Sie die Funktion mit dem richtigen Zeitvektor an der x-Achse. Hierzu können Sie einfach `k` durch die Samplingrate teilen, also `ax[0].plot(k/sample_rate,x)`
6. Testen Sie Ihr Ergebnis indem Sie in die Funktion zoomen und überprüfen ob die Länge einer Periode der Frequenz 200 Hz entspricht.

[zurück](#C1)

<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.2:
</a>

1. Bestimmen Sie die Länge einer Periode bei 200 Hz als Grundfrequenz und einer Abtastrate von 16000 Hz. 
2. Nutzen Sie aus, dass `np.arange` auch negative Startwerte annimmt und definieren Sie den Zeitvektor `k =` einer Periode von `-L = -P/2` bis `L = P/2 -1`. Ist `k` so lang, wie Sie es wollten? `print(len(k))` 
3. Berechnen Sie eine Periode `one_period = ...` mit Hilfe der gegebenen Formel. `np.abs` implementiert die Betragsstriche.
4. Plotten Sie das Ergebnis. Ist es eine Dreiecksfunktion? Jeden Zwischenschritt überprüfen. Dies ist immer ein wichtiger Schritt. 
5. Berechnen Sie wie viele Wiederholungen Sie benötigen um 10ms zu füllen. 
6. Suchen Sie nach einer Funktion, die ein numpy array wiederholt, z.B `python repeat a vector`. Lösungen könnten sein `repeat, tile, repmat`. Schauen Sie genau was die Funktionen mit dem Array macht und wiederholen Sie ihre einzelne Periode `x_tri = ....`.


[zurück](#C1)

<a name="L13">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>

1. Kapseln Sie die Erzeugung des Sinus aus der ersten Aufgabe in eine Funktion und rufen Sie diese auf zum testen.
``` python
def get_sinus(f0, sample_rate, len_seconds, amplitude = 1):
    # your code


    # x = np.sin(.....)
    return x

f0 = 200
fs = 16000
len_s = 0.01
amp = 2

sin_sig = get_sinus(f0,fs,len_s,amp)

# und nun der plot code
```
2. Schreiben Sie eine weitere Funktion, die das Vorzeichen für jeden Wert zurückgibt. `x_vor = my_sign(signal)` und nutzen in dieser Funktion die Funktion `np.sign`. Dies ist nur eine Zwischenlösung, da wir nur sehen wollen, ob alles geht.
3. Plotten Sie den Ausgang von `sign(sin(x))`. Sehen Sie eine Rechteckfunktion mit der richtigen Frequenz? 
4. Problem: Für 0 gibt `np.sign` Null zurück. Wir wollen aber eigentlich eine 1 haben. Eine mögliche Lösung zeigt zusätzlich einige tolle Eigenschaften von Python. In einem Array können Sie auf die Elemente mit eckigen Klammern und einem Index zugreifen `x[index]`. Sie können aber auch den Index währen des Zugriffs berechnen lassen, z.B auf alle Werte in einem Vektor < 0 `x[x<0]`. Mit einer Zuweisung erhalten wir so die Möglichkeit eine einfache Sign Funktion zu schreiben. Die negative Seite ist `x[x<0] = -1` . Die positive Seite bekommen Sie hin.

[zurück](#C1)


<a name="L21">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.1:
</a>

1. Schauen Sie bei der ersten Aufgabe wie das geht, da ist dies eine Zwischenaufgabe.

[zurück](#C2)

<a name="L22">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.2:
</a>

1. Es gibt viele Möglichkeiten um zu testen, welche Form eine bestimmte Variable hat. Numpy Arrays haben die Eigenschaft `shape`. Bei 1 dimensionalen Vektoren bietet sich die Funktion `len(...)` an.
2. Mit einer `if` Abfrage können Sie die beiden Möglichkeiten unterscheiden.
3. Vergessen Sie nicht, eigentlich brauchen Sie das nicht

[zurück](#C2)


<a name="L23">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.3:
</a>

1. Bei Schleifen iterieren Sie immer. Sie können über Zahlen iterieren (`for k in range(ZahlIter:)` ) oder über Listen bzw. Arrays `for one_amp in amplitiudes:`. Aber es gibt auch die Möglichkeit die Elemente und einen Counter zu erhalten durch Nutzung von `enumerate`. Suchen Sie hierfür die Beschreibung und Beispiele und nutzen dies für das Amplituden Array.
2. In der Schleife addieren Sie die erzeugten Sinuston mit der gewünschten Amplitude. Am einfachsten ist es, vor der Schleife einen Nullvektor gleicher Länge zu erzeugen und mit `+=` die neuen Anteile aufzuaddieren.
3. Testen Sie ihre Funktion zunächst mit ganz einfachen Aufrufen `amplitude = [0,1]` und schauen Sie ob die erzeugte Schwingung die doppelte Frequenz hat. Immer wieder jeden Schritt testen. 

[zurück](#C2)

<a name="L24">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.4:
</a>

1. Rechteckschwingungen haben nur ungerade Harmonische, somit muss im Amplitudenvektor jeder zweite Wert Null sein.
2. Die Amplituden nehmen mit `1/n` ab, mit n der aktuellen Nummer der Harmonischen.


[zurück](#C2)

<a name="L25">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.5:
</a>
1. Beginnen Sie mit `ampl = [1,0,1/3]` und erhöhen Sie die Anteile langsam und plotten anschließend. Die Rechteckform sollte sichtbar werden. Es bleiben aber immer Überschwinger am Rand (Gipps-Phänomen).
2. 

[zurück](#C2)
