# Python Übung 3: Systeme Differenzengleichung

## Aufgaben F-Level: 

<a name="F1"></a>

1. Erzeugen Sie ein weißes gaußverteiltes Rauschen der Länge 1s mit einer Samplingrate von 16000 Hz. Berechnen Sie den Ausgang sampleweise für das System mit der Differenzengleichung $`y(k) = b_0 x(k) + b_1 x(k-1)`$ mit $`b_0 = 0.5`$ und $`b_1 = 0.5`$. Hören Sie sich das Ergebnis an und beurteilen Sie, was dieses System tut. Ändern Sie $`b_1`$ auf -0.5 und wiederholen die Analyse.
    [Tipps für F1](#C1)

<a name="F2"></a>

2. Nutzen Sie das gleiche Rauschen, aber implementieren Sie diesmal die Differenzengleichung $`y(k) = b_0 x(k) + b_1 x(k-1) - a_1 y(k-1)`$. Im diesem Versuch ist $`b_0 = 1`$ und $`b_1 = 0`$. Setzen Sie im einer Schleife $`a_1`$ von -0.9 bis 0.9 in Schritten von 0.3 und erzeugen Sie die Ausgangssignale. Was hören Sie? 
    [Tipps für F2](#C2)

<a name="F3"></a>

3. Ersetzen Sie die eigene Schleife zur Berechnung des Ausgangs mit dem `lfilter` Befehl. Überprüfen Sie ob die Ergebnisse gleich sind.
    [Tipps für F3](#C3)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Erzeugen Sie das Rauschsignal (Tipps siehe Übung 1) 
2. Programmieren Sie eine Schleife über alle Samples, die zunächst nur den Eingang auf den Ausgang kopiert (evtl. schon mit b0 multiplizieren) [Tipps für C12](#L12)
3. Sie benötigen für $x(k-1)$ einen Zwischenspeicher. Diese werden oft als Zustandsvariable (States) bezeichnet. Überlegen Sie sich, wann Sie diesen zuweisen müssen. [Tipps für C13](#L13)
4. Bauen Sie mit dem Zwischenspeicher (Zustandsvariable) und dem Eingang die Differenzengleichung auf. [Tipps für C14](#L14)
5. Speichern Sie das Ausgangssignal in einem File (Nutzen Sie SoundFile). Achten Sie darauf, ob es Übersteuerungen (Clipping) gibt. Falls ja, skalieren Sie das Signal durch eine Division durch das Maximum. [Tipps für C15](#L15)
6. Hören Sie sich das Ergebnis mit einem beliebigen Soundprogramm an (z.B. Audacity). Falls die Soundkarte nicht mit $f_s = 16000$ arbeiten, ändern Sie ihr Programm mit $f_s = 44100$. [Tipps für C16](#L16)
7. Versuchen Sie die Soundausgabe direkt in Python (`pip install SoundCard`). [Tipps für C17](#L17)

[zurück zu F1](#F1)

<a name="C2">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 2:
</a>

1. Speichern Sie das Programm aus Aufgabe 1 unter einem anderen Namen und kommentieren die Soundausgabe zunächst aus. [Tipps für C21](#L21)
2. Definieren Sie einen weiteren Zwischenspeicher für den Zustand (State) des Ausgangs. Sie brauchen auch für die rekursiven Anteile jeweils einen Zwischenspeicher.[Tipps für C22](#L22)
3. Überlegen Sie sich genau die Zuweisungen zu den Zwischenspeichern [Tipps für C23](#L23)
4. Überprüfen Sie zunächst von Hand ($a_1$ händisch ändern) ob Sie für 0.9 und -0.9 einen Unterschied hören [Tipps für C24](#L24)
5. Programmieren Sie eine Schleife um die innere Signalgenerierung und überlegen Sie, wie man den Filename zum abspeichern von der Schleifenvariable abhängig gestalten kann, damit alle Signale auch gespeichert werden. (Sonst überschreiben Sie immer das aktuelle Signal) [Tipps für C25](#L25)
6. Versuchen Sie die Soundausgabe zu nutzen. Sie müssten 7 Rauschsignale hören. [Tipps für C26](#L26)

[zurück zu F2](#F2)

<a name="C3">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 3:
</a>

1. Schauen Sie in der Referenz nach `scipy.signal.lfilter` [Tipps für C31](#L31)
2. Überlegen Sie sich wie in ihrem Bsp die a und b Vektoren definiert sind [Tipps für C32](#L32)
3. Ersetzen Sie Ihre innere Schleife über die Samples mit lfilter. Der Code wird deutlich übersichtlicher, da sich lfilter auch um die Zustandsspeicher kümmert. [Tipps für C33](#L33)


[zurück zu F3](#F3)

Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1

<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.2:
</a>


1. Sie kennen die Anzahl an Samples. Eine reine Zählschleife wird in Python durch `for kk in range(N):` gelöst.
2. Definieren Sie Ihren Ausgangsvektor vor der Schleife. Es gibt den Befehl `zeros_like` der Ihnen ermöglicht ein leeres numpy Array mit den gleichen Dimensionen eines anderen Arrays aufzubauen. 
3. Der Indexzugriff erfolgt immer mit eckigen Klammern `out[kk] = ...`

[zurück zu C1](#C1)


<a name="L13">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>


1. Sie müssen immer davon ausgehen, dass ihr System für $`k<0`$ in Ruhe ist. Initialisieren Sie deshalb Ihre Zustandsvariable (State) mit Null. Überlegen Sie wo im Code diese Initialisierung stehen muss. 
2. Es ist immer sinnvoll den Namen des States an den Koeffizienten zu koppeln mit dem die Zustandsvariable multipliziert wird. Dies erhöht die Lesbarkeit. Bsp `state_b1`.
3. Die Zustandsvariable speichert den letzten Eingangswert `x(k-1)`. Wann darf diese Zuweisung erst erfolgen, damit die Ausgangsgleichung `out[kk] = ` nicht falsch wird.

[zurück zu C1](#C1)

<a name="L14">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.4:
</a>


1. Sie können mit den eckigen Klammern direkt auf den Eingang und Ausgang zugreifen. Daher ist diese Aufgabe nur ein abschreiben, nur das `x(k-1)` durch die Zusatandsvariable ersetzt werden muss. 

[zurück zu C1](#C1)

<a name="L15">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.5:
</a>


1. Importieren Sie oben im Code SoundFile mit `import soundfile as sf`. Falls SoundFile noch nicht bekannt ist, haben Sie vergessen die Bibliothek zu installieren. `pip install SoundFile`. 
2. Für das speichern von Files benötigen Sie immer einen Filenamen, z.B. `fileout_name = 'outFilteredNoise.wav'` und natürlich eine Samplingrate. 
3. Der eigentliche Schreibbefehl ist `sf.write(fileout_name, data, samplerate)`
4. Um Clipping zu verhindern, gibt es sehr unterschiedliche Methoden. Welche man nutzt hängt stark davon ab, ob man eine feste Kalibrierungsgröße benötigt. Ist dies der Fall, definiert man eine eine konstante Abschwächung (z.B. -35dB). In diesem Fall ist dies nicht notwendig und eine relative Normalisierung reicht aus.
5. Normalisieren lässt sich ein Signal indem man durch das Maximum des Absolutwerts dividiert. Beide Operatoren sind Funktionen in numpy.
6. Es ist sinnvoll, zunächst nur den Maximalwert (nicht die Absolutwertbildung vergessen) zu bestimmen um evtl bei Werten kleiner als 1, auf die Normalisierung zu verzichten.
   

[zurück zu C1](#C1)
<a name="L16">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.6:
</a>


1. Wesentlich ist immer die Daten auf der Festplatte wieder zu finden. Es kann sinnvoll sein, beim speichern nicht nur den Filename anzugeben, sondern auch den Pfad. z.B. (in Windows) `fileout_name = 'c:\temp\mein_file.wav'`, wenn es ein Unterverzeichnis `temp` gibt.
 

[zurück zu C1](#C1)

<a name="L17">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.7:
</a>

1. SoundCard bietet zwei Modi für die Ausgabe. Blockorientiert (benötigen Sie hier nicht) und ganze Arrays.
2. Suchen Sie im Internet nach `python SoundCard`. `PyPy` und `GitHib` zeigen das Tutorial. 

[zurück zu C1](#C1)


### Aufgabe 2




<a name="L21">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.1:
</a>

1. Überlegen Sie beim speichern mit neuem Namen, immer über den Namen nach. `Aufg2` ist kein guter Name. Sie finden die Lösung nicht wieder. 
2. Auskommentieren von Zeilen mit `#` ist immer nur eine Not- und Zwischenlösung und gehören nie zu einem fertigem Programm. Ausnahmen müssen sehr gut begründet sein. 

[zurück zu C2](#C2)

<a name="L22">
<a name="L23">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.2 und 2.3:
</a>
</a>

1. Nutzen Sie die Erkenntnisse aus der Aufgabe 1. Benennung der Zustandsvariable nach der beabsichtigten Multiplikation. Wo gehört die Initialisierung mit Null hin. Wann müssen Sie `state_a1 = ...` schreiben.

[zurück zu C2](#C2)



<a name="L24">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.4:
</a>

1. Speichern Sie die Dateien mit einem geeigneten Zusatz. Um den Punkt zu vermeiden, nutzen Sie das 10fache als guten Indikator. Bsp `out_9.wav` 
2. Sie sollten deutliche Unterschiede hören, wenn Sie eine Normalisierung mit dem Maximum durchführen (heller vs dumpfer)

[zurück zu C2](#C2)


<a name="L25">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.5:
</a>

1. Es gibt viele Wege eine Zahl in eine Stringvariable zu überführen. Suchen Sie nach geeigneten Möglichkeiten und testen diese zunächst, um zu sehen, ob Ihre Idee funktioniert. Ein Test könnte so aussehen.
```python

Zahl = 0.9
text = 'out_' + str(int(Zahl*10)) + '.wav'
print(text)
```
2. Eine bessere Lösung könnte über die moderneren `f''` Strings funktionieren.  Hier ist eine leichte Erklärung https://matthew-brett.github.io/teaching/string_formatting.html
3. Bauen Sie als nächsten einen Vektor mit den gewünschten Werten. `np.linspace` bietet sich an.
4. Die Schleife sollte in diesem Fall wieder mit `for kk, a_value in enumerate(IhrVektor)`
5. Nutzen Sie die neuen Erkenntnisse, um zunächst für eine Schleife die Namen der Ausgangsfiles zu generieren und mit print auszugeben. Dies ist nur ein weiterer Test. 
6. Setzen Sie abschließend die eigentliche Signalgenerierung in die Schleife und hören sich die Signale an. Sie müssten eine gleichmäßige Änderung von -0.9 bis 0.9 wahrnehmen.

[zurück zu C2](#C2)


<a name="L26">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.6:
</a>

1. Sie können die Soundausgabe mit SoundCard jeweils nach der Signalgenerierung starten. Falls es dann zu überlappende Signale kommt, müssen Sie eine Pause einfügen. Suchen Sie im Netz nach einer geeigneten Möglichkeit.


[zurück zu C2](#C2)



### Aufgabe 3

<a name="L31">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 3.1:
</a>

1. Aus der Referenz wird deutlich, dass `lfilter` die LTI-Differenzengleichung implementiert. Übergeben wird der Eingangsvektor, also unser Rauschen und zwei Vektoren mit den transversalen b-Koeffizienten und den rekursiven a-Koeffizienten. Zu beachten ist, dass die Subtraktion der rekursiven Anteile nicht die Festlegung der Koeffizienten berührt. 

[zurück zu C3](#C3)


3. Ersetzen Sie Ihre innere Schleife über die Samples mit lfilter. Der Code wird deutlich übersichtlicher, da sich lfilter auch um die Zustandsspeicher kümmert. [Tipps für C33](#L33)

<a name="L32">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 3.2:
</a>

1. Der b-Vektor `b = np.array([....])` hat in unseren vorherigen Beispielen nur 2 Elemente. Der a Vektor auch, da immer gilt $`a_0 = 1`$.

[zurück zu C3](#C3)

<a name="L33">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 3.3:
</a>

1. Eine gute Möglichkeit zu debuggen kann auch sein, Code parallel zu nutzen und erst später zu löschen.
2. Bauen Sie eine neue Ausgangsgröße auf z.B. `out_lfilter = sig.lfilter(....)` und vergleichen dann die vorherige Lösung.
3. Wenn Sie sicher sind, dass die Lösungen äquivalent sind, löschen Sie die alte umständliche Lösung.

[zurück zu C3](#C3)


