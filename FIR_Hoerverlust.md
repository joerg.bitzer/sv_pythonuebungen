# Python Übung 4: Titel

## Aufgaben F-Level: 

<a name="F1"></a>

1. Entwerfen Sie ein FIR Filter, dass den linearen Hörverlust eines Schwerhörigen simuliert und hören Sie sich das Ergebnis an. Dazu sollen die Werte des Hörverlust für die Frequenzen: 250Hz, 500Hz, 1000Hz,  2000Hz, 4000Hz, 8000Hz vorgegeben werden:

Beispiel:

250Hz 0dB

500Hz -3dB

1000Hz -10dB

2000Hz -20dB

4000Hz -20dB

8000Hz -50dB 
    [Tipps für F1](#C1)


Falls Sie bei der Aufgabe Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Nachdenken, wie die angegeben Werte in ein FFT Raster zu übertragen sind.   [Tipps für C1](#L11)
2. Fenstermethode zum Design der b-Koeffiziente nutzen. Filterlänge sollte ein freier Parameter sein. [Tipps für C1](#L12)
3. Anhören des Ergebnisses.[Tipps für C1](#L13)

[zurück zu F1](#F1)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1

<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

[zurück zu C1](#C1)


<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>


[zurück zu C1](#C1)

<a name="L13">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>


[zurück zu C1](#C1)

