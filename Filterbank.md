# Python Übung 4: Titel

## Aufgaben F-Level: 

<a name="F1"></a>

1. Aufgabe Filterbank:

Ein wichtiges Element in Hörgeräten und in der Studiotechnik ist die Filterbank. Sie zerlegt das breitbandige Eingangssignal in einzelne Bänder (in Hörgeräten oft (wie ich finde fälschlicherweise) Kanäle genannt).

Entwerfen Sie eine zwei-kanalige Filterbank mit der Grenzfrequenz fs/16 (bei fs = 48000 Hz). Hierzu müssen Sie ein Tief und Hochpasspaar entwerfen. Nutzen Sie zunächst einen Butterworth-Entwurf erster Ordnung. Zeigen Sie, dass die Addition der beiden Bänder erneut alle Frequenzen gleich durchlässt. Wiederholen Sie das Experiment mit Butterworth-Filtern höherer Ordnung. Was erkennen Sie? 

Zusatz: Nutzen Sie das Tiefpass und Hochpass Filter jeweils zweimal und schauen sich dann das Ergebnis an. Was erkennen Sie nun? Dieser Ansatz entspricht einem Butterworth zum Quadrat Ansatz und wird Linkwitz-Riley Filter genannt. Dieser kommt vor allem bei Frequenzweichen im Lautsprecherbau vor.

    [Tipps für F1](#C1)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Schauen Sie sich die Dokumentation zum Butterworth-Design genau an. Berechnen Sie die Filterkoeffizienten `b_TP, a_TP, b_HP, a_HP`.
2. Schauen Sie sich mit `freqz` die Übertragungsfunktionen an. Am Besten in einem Plot.
3. Es gibt zwei Wege die Addition der beiden Bänder anzusehen. Im Zeitbereich durch Überlagerung der beiden Impulsantworten. Hierzu nutzen Sie die Koeffizienten mit einem Delta-Impuls und dem `lfilter`-Befehl.
4. Oder Sie addieren die komplexen Übertragungsfunktionen und plotten den Betrag des Ergebnis. Am Besten in das gleiche Bild wie die Einzelfilter.
5. Ändern Sie nun die Ordnung (2,3,4,5,6) (Also als Schleife programmieren)
6. Ist das Ergebnis tatsächlich immer eine saubere Zerlegung, so dass sich HP und TP Anteil perfekt ergänzen?
7. Zusatz: Überlegen Sie für sich, was es heißt, einen Filter zweimal anzuwenden im Zeitbereich und im Frequenzbereich. 
8. Was zeigen die Ergebnisse für die unterschiedlichen Ordnungen nun. Stimmt eigentlich noch die Ordnungsangabe?

[zurück zu F1](#F1)

Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1

<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

[zurück zu C1](#C1)


<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>


[zurück zu C1](#C1)

