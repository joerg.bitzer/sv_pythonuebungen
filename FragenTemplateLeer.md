# Python Übung 4: Titel

## Aufgaben F-Level: 

<a name="F1"></a>

1. F1
    [Tipps für F1](#C1)

<a name="F2"></a>

2. F2
    [Tipps für F2](#C2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>


[zurück zu F1](#F1)

<a name="C2">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 2:
</a>


[zurück zu F2](#F2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1

<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

[zurück zu C1](#C1)


<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>


[zurück zu C1](#C1)


### Aufgabe 2


<a name="L21">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.1:
</a>


[zurück zu C2](#C2)

<a name="L22">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.2:
</a>

1. Nutzen Sie die Erkenntnisse aus der Aufgabe 1. Benennung der Zustandsvariable nach der beabsichtigten Multiplikation. Wo gehört die Initialisierung mit Null hin. Wann müssen Sie `state_a1 = ...` schreiben.

[zurück zu C2](#C2)


<a name="L23">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.3:
</a>


[zurück zu C2](#C2)



