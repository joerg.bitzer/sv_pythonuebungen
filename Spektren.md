# Python Übung 5: Spektren

## Aufgaben F-Level: 

<a name="F1"></a>

1. Zeichnen Sie in eine Abbildung, das Spektrum (nutzen Sie `np.fft`) einer Sinusschwingung (f = 456 Hz, fs = 16000) und einer Rechteckschwingung (mit sign(sin())) (f = 734, fs = 16000) mit einer Signallänge von 0.25s. Achten Sie darauf, dass die x-Achse die echten Frequenzen anzeigt (Hineinzoomen um ganz sicher zu sein). Plotten Sie nur bis zur Nyquistfrequenz. Wie erklären Sie das Spektrum des Rechtecks?
    [Tipps für F1](#C1)

<a name="F2"></a>

2. Gegeben ist das Signal "test.wav" (im repository). Mit welcher Frequenz wurde das Sinus-Signal erzeugt? Nutzen Sie bei der Analyse die FFT mit unterschiedlichen Längen (zero-padding). Zusatz: Versuchen Sie die Frequenz direkt aus dem Spektrum zu berechnen, also nicht von Hand und Auge abzulesen. Die Erhöhung der Genauigkeit, kann mittels parabolischer Interpolation erfolgen (mal DuckDuckGoen) 
    [Tipps für F2](#C2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-Overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Erzeugen Sie zunächst ein plot mit 2 Zeilen (`nrows = 2`). Das kennen Sie aus den ersten Übungen. Keine Tips, da aus alter Übung bekannt.

2. Erzeugen Sie den Sinus. Nutzen Sie die Sinusgeneratoren, die Sie aus den vorherigen Übungen kennen.
    Keine Tips, da aus alter Übung bekannt.

3. Berechnen Sie das Spektrum mit einer geeigneten Auflösung. Nutzen Sie `np.fft.fft` und `np.fft.rfft`. Was ist der Unterschied? Wie viele Samples sollten Sie nutzen? Testen Sie unterschiedliche Längen. Schauen Sie sich diese Funktion an: https://gist.github.com/lppier/a59adc18bcf32d8545f7 Warum ist das nützlich?
    [Tipps für C13](#L13)

4. Bauen Sie einen Frequenzvektor auf, der zu Ihrem Spektrum passt (`rfft` ist die bessere Wahl).[Tipps für C13](#L14)
5. Plotten Sie das Spektrum des Sinus und zoomen hinein. Stimmt die Frequenz überein? Keine Tips. Plotten mit x und y Vektoren können Sie schon sicher.
6. Wiederholen Sie die Schritte mit dem Rechteck. Die Lösung für sign wurde ebenfalls in einer vorherigen Übung gelöst. [Tipps für C13](#L16)
7. Schauen Sie sich die Frequenzen in der Reihenfolge ihrer maximalen Amplituden an. Welchen Zusammenhang finden Sie zur Samplingrate. Variieren Sie die Frequenzen (ein wenig höher, ein wenig verringern). [Tipps für C13](#L17)

[zurück zu F1](#F1)

<a name="C2">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 2:
</a>

1. Laden Sie die Datei ein und achten Sie darauf, dass sie beide Rückgabewerte nutzen (fs).
    Keine Tips, haben wir schon häufiger gemacht.

2. Berechnen Sie das Spektrum mit unterschiedlichen FFT-Längen (kürzer als das Signal, richtige Länge, viel länger)
    [Tipps für C22](#L22)
3. Bauen Sie den passenden Frequenzvektor jeweils auf und plotten Sie die Signale.
    siehe vorherige Aufgabe
4. Lesen Sie die Frequenz ab. 
    keine Tips, da nur zoomen und ablesen. 


[zurück zu F2](#F2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1

<a name="L13">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

1. Wenn Sie das ganze Signal nutzen, haben Sie typischerweise keiner 2er Potenz mehr.
2. Die Funktion in dem Link errechnet Ihnen die notwendige 2er Potenz, um die FFT Größe zu bestimmen. Diese ist immer noch sehr groß.
3. Um eine beliebige Größe in den FFT Funktionen zu nutzen, müssen Sie als 2. Parameter `n = ...` angeben.
4. Mit diesem Parameter können Sie auch kürzere FFTs ausprobieren. Schauen Sie auf den Effekt.
5. Schauen Sie sich die Länge der zurückgegebenen Arrays von `fft` und `rfft` an.

[zurück zu C1](#C1)


<a name="L14">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>

1. Nutzen Sie `linspace`. Die Länge des Vektors kennen Sie durch die Rückgabe. Der Befehl `len` kann helfen, ist aber eigentlich nicht notwendig, wenn Sie in Ihrem Skript vorne eine Variable `fftsize` einführen. Nutzen Sie `rfft` haben Sie `fftsize/2 + 1` Spektralwerte im Bereich von 0 bis `fs/2`. Damit haben Sie auch die anderen Paremeter.
    

[zurück zu C1](#C1)

<a name="L16">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>

1. Nutzen Sie erneut zunächst den Sinusgenerator
2. Plotten Sie das Spektrum zum Sinus
3. Dann ändern Sie das Inputsignal zum Rechteck.

[zurück zu C1](#C1)
<a name="L17">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>

1. Dies ist wirklich eine Aufgabe zum ausprobieren. Ändern Sie die Frequenzen so, dass die Samplingrate eine Vielfache der Testfrequenz ist. 
2. Sie könnten jetzt einen Faktor aufbauen, den Sie langsam variieren, also `f0 = fs/32*detune_factor`. Diesen Faktor variieren Sie von 0.9 bis 1.1.
3. Wenn Sie davon 5 Bilder untereinander plotten also 0.9 0.95 1 1.05 1.1 sollten Sie den Zusammenhang deutlich sehen können.

[zurück zu C1](#C1)

### Aufgabe 2


<a name="L22">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.1:
</a>

1. Wesentlich ist der zweite Parameter `n=` bei der FFT-Routine. Mit diesem können Sie die Aufgabe direkt lösen.
2. Definieren Sie einen Vektor mit den unterschiedlichen Längen. Plotten Sie diese untereinander in eine Figure. Dadurch sieht man die Effekte am Besten.
   

[zurück zu C2](#C2)


