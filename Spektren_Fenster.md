# Python Übung 4: Titel

## Aufgaben F-Level: 

<a name="F1"></a>

1. Programmieren Sie ein Signal der Länge 0.3s, dass aus 2 Sinustönen besteht (fs = 16 kHz). Die Frequenzen des ersten Tones können die folgendne Werte annehmen 90 Hz, 500 Hz, 1723 Hz. Die Leistung soll -3dBFS betragen. Der zweite Ton kann die folgenden Frequenzen beinhalten: 1837 Hz, 2000 Hz, 6143 Hz. Die Leistung soll bei -10 dBFS oder -60dBFS liegen. Berechnen Sie das Spektrum mit einer FFT-Länge von 256, 1024 und 4096 Samples und den Fenstertypen blackmanharris, boxcar (rect), von Hann and Hamming (siehe `scipy.windows`). Bei welchen Kombinationen können Sie die Sinustöne im Spektrum erkennen und die relativen Leistungsunterschiede ablesen. Das Ergebnis ist eine Tabelle mit `3*3*2*3*4 = 216` Antworten.  OK, evtl brauchen Sie nicht alle, aber evtl. können Sie die Erkenntnisse in wenigen Sätzen zusammen fassen.
   
    [Tipps für F1](#C1)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Programmieren Sie oder nutzen Sie die schon programmierte Funktion zur Erzeugung eines Sinus.
2. Überlegen Sie sich, wie Sie das zu analysierende Signal aufbauen können. Nutzen Sie bei allem Variablen und keine Magic Numbers, da am Ende alles über Schleifen laufen sollte.
3. Überlegen Sie sich, wie Sie an die Fensterfunktionen kommen. Schreiben Sie eine Funktion, die Ihnen die Fensterfunktionen zurückgibt, wenn Sie Kurznamen übergeben. Beispielsweise  könnten Sie final über die folgende Liste test_windows = ['rect', 'Hann', 'Hamming', 'bmh'] iterieren.
4. Überlegen Sie sich, welche Bilder gemeinsam angezeigt werden können, oder welche Plots in ein Bild passen. Sie könnten sich dazu entscheiden, alle Bilder einzeln zu zeigen, aber alle 4 Fenster in einem Plot oder in 4 subplots.
5. Überlegen Sie sich alle Listen über die Sie iterieren wollen, also f1 = [], f2 = [], gain1 = [], gain2 = [] usw.
6. Schreiben Sie zunächst nur die innerste Schleife, anschließend die nächste weiter außen usw., bis Sie über alle Kombinationen iterieren.

[zurück zu F1](#F1)



Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1

<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

[zurück zu C1](#C1)


<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>


[zurück zu C1](#C1)


