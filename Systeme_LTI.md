# Python Übung 4: Systeme LTI

## Aufgaben F-Level: 

<a name="F1"></a>

1. Testen Sie durch Analyse des Ausgangssignals für welchen Bereich von $`a_1`$ das folgende System stabil ist.
$`y(k) = 0.5 x(k) - a_1 y(k-1) - 0.75 y(k-2)`$
    
[Tipps für F1](#C1)

<a name="F2"></a>

2. Versuchen Sie die Faltung von $`x(k) = \delta(k) - 2 \delta(k-1)`$ mit $`h(k) = -\delta(k)  + 2 \delta(k-2)`$ als Mini-Filmsequenz (Ablauf von Bildern) zu visualisieren. Berechnen Sie zunächst das Ergebnis und benutzen die Standard-Plotbefehle. 
    [Tipps für F2](#C2)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Nehmen Sie die Lösung von letzter Woche mit `lfilter` und setzen im rekursiven Vektor a zunächst a_1 auf 0 . [Tipps für C1.1](#L11)
2. Prüfen Sie den den Ausgang, indem Sie die Impulsantwort der ersten 5 Elemente von Hand ausrechnen und anschließend mit ihrem Programm. Der Input ist also nicht die Rauschfolge, sondern ein Deltaimpuls der Länge 5. [Tipps für C1.2](#L12)
3.  Suchen Sie nach einem a_1, dass sicher zu einem instabilem System führt (alle Werte im Betrag größer 2 sind immer instabil bei einem System 2. Ordnung). [Tipps für C1.3](#L13)
4.  Instabile Systeme führen im Ausgangssignal (im Betrag) zu einem immer größeren Signal. Wie kann man das nutzen, um ein stabiles von einem instabilen System zu unterscheiden. Gehen Sie pragmatisch vor. Ihre Lösung muss nicht für theoretische Grenzfälle funktionieren. [Tipps für C1.4](#L14)  
5.  Schreiben Sie eine Schleife in der Sie a_1 von -2 bis 2 in kleinen Schritten verändern und testen jeweils auf Stabilität. [Tipps für C1.5](#L15)
6.  Zusatz: Testen Sie diesen Bereich auch für a_2 = 0.5 und a_2 = -0.5. Den Bereich für a_2 = 0 kennen Sie ja.  [Tipps für C1.6](#L16)

[zurück zu F1](#F1)

<a name="C2">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 2:
</a>

1. Um das Ergebnis zu kennen, nutzen Sie den Befehl `numpy.convolve` und plotten das Ergebnis mit `matplotlib.pyplot.stem`.  [Tipps für C2.1](#L21)
2. Überlegen Sie sich, wie Sie auf der x-Achse auch negative Zeitindices plotten können. Sie benötigen dies für die zeitlich gespiegelte Folge. [Tipps für C2.2](#L22)
3. Überlegen Sie genau, was Ihr Minifilm zeigen soll. Erzeugen Sie zunächst nur einzelne Plots (Es werden dann eben einige). Wie man das zum Film zusammenbaut kommt später. [Tipps für C2.3](#L23)
4. Zunächst muss die zeitliche Spiegelung gezeigt werden. [Tipps für C2.4](#L24)
5. Anschließend plotten Sie die zweite Folge in einer anderen Farbe (immer `stem` nutzen) [Tipps für C2.5](#L25)
6. Verschieben Sie die Folge und plotten in einem zweiten subplot das Ergebnis der Faltung Schritt für Schritt. [Tipps für C2.6](#L26)
7. Suchen Sie nach Möglichkeiten aus einzelnen Plots einen Film zusammen zu bauen.
[Tipps für C2.7](#L27)

[zurück zu F2](#F2)

Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

### Aufgabe 1


<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

1. Damit Sie `lfilter` nutzen können, müssen Sie die beiden Koeffizientenvektoren, die wir immer b und a nennen bestimmen.
2. b ist einfach, da wir nur einen transversalen Koeffizienten haben. Somit hat der Vektor nur einen Eintrag
3. a hat diesmal 3 Einträge. Die 1 vorne, da wir ja $`a_0` = 1$ setzen. Wenn Sie es hier schon richtig gut machen wollen, definieren Sie sich zwei Variablen $`a_1`$ und $`a_2`$ und bauen sich den a Vektor aus diesen zusammen. Also `a = [1, ...]`.   
4. Nun können Sie `lfilter` nutzen und sich den Ausgang ansehen. Als Eingang können Sie zunächst Rauschen nehmen oder Sie definieren sich einen Deltaimpuls.
[zurück zu C1](#C1)


<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.2:
</a>

1. Das von Hand ausrechnen ist für `a_1 = 0` besonders einfach.
2. Definieren Sie sich nun einen Deltaimpuls. Schreiben Sie am Besten eine kleine Funktion.
```python
def get_deltaimpulse(len_samples):
    delta = np.zeros(....)
    delta[0] = ...
    return delta
```
3. Berechnen Sie nun mit dem Deltaimpuls als Eingang und `lfilter` den Ausgang und prüfen ob Ihre Lösungen übereinstimmen.

[zurück zu C1](#C1)

<a name="L13">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>

1.  Mit dem Wissen, dass alle $`|a_1| > 2`$ immer zu einem instabilen System führen. wählen Sie z.B. $`a_1 = 3`$. 
2.  Testen Sie mit einem etwas längerem Deltaimpuls, ob die Impulsantwort größer wird. Am Besten plottet man die Impulsantwort.


[zurück zu C1](#C1)
<a name="L14">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.4:
</a>

1. Dies ist sicher der schwierigste Teil der Aufgabe. Ein pragmatischer Ansatz kann sein, dass man eine etwas längere Impulsantwort berechnet (vielleicht mit 1000 Werten) und anschließend schaut, ob bei den letzten 100 Werten Ausgangswerte größer 1 herauskommen. Diesen Ansatz erst mal testen.
2. Erzeugen Sie einen Deltaimpuls mit 1000 Werten (gut das wir uns eine Funktion geschrieben haben, die wir jetzt nutzen können) und schicken diesen durch das System mit $`a_1 = 3´$.
3. Aus dem Ausgang extrahieren wir die letzten 100 Werte. Das tolle an Python ist, dass man mit negativen Indices arbeiten darf. Der 100. Wert vor dem letzten ist deshalb $`y[-100]`$. Mit dem `:-Operator` können wir also die letzten 100 Werte extrahieren. 
4. Um nun zu schauen, ob ein einziger Betragswerte größer als 1 ist, gibt es viele Möglichkeiten. Eine langsame wäre eine Schleife zu programmieren (Nicht sehr pythonic). Suchen Sie nach $`any`$ als kompakte Lösung ohne Schleife (Ergebnis ist eine boolsche Variable, mit den Werten `True` oder `False`). 
5. Testen Sie Ihre Methode mit $`a_1 0 = 0`$. Es muss herauskommen, dass kein Wert größer als 1 ist (False). 
6. Kapseln Sie Ihre Methode in einer Funktion 
```python
def is_stable_LTISystem(b,a):

    is_stable = ....
    return is_stable
```
7. Und erneut testen, ob immer noch das richtige herauskommt für $`a_1 = 0`$ und $`a_1 = 3`$.

[zurück zu C1](#C1)
<a name="L15">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.5:
</a>
1.  Schreiben Sie eine Schleife in der Sie a_1 von -2 bis 2 in kleinen Schritten verändern und testen jeweils auf Stabilität. 

1. Bauen Sie einen Vektor auf für die möglichen Werten von $`a_1`$. Nutzen Sie zB `np.arange`.
2. Programmieren Sie eine Schleife mit den einzelnen Werten von $`a_1`$. Nutzen Sie `enumerate`.
3. Bauen Sie den a Vektor immer neu auf, rufen Ihre Testfunktion auf uns speichern den Ausgang in einer Liste. Hier hilft es sehr eine leere Liste vor der Schleife zu definieren `ResultList = []` und mit `append` jeweils ein neues Element anzufügen.
4. Anschließend können Sie in `ResultList` nach dem Index des ersten und letzten `True` suchen und können mit dem Index nachsehen in welchem Bereich $`a_1`$ stabil ist. Suchen Sie nach `np.where(...)` für die Suche. Nutzen Sie oft `print` um Zwischenergebnisse anzusehen. Nutzen Sie `shape` und den Debugger um herauszufinden, warum das Ergebnis nicht mit einem einfachen Indexzugriff funktioniert. Dies ist eine häufiges Artefakt der `append` Lösung. Zweidimensionale Zugriffe erfolgen über `x[][]`.

[zurück zu C1](#C1)
<a name="L16">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.6:
</a>

1. Testen Sie ihr System auch für die anderen $`a_2`$. Bei $`a_2 = 0`$ muss ein Bereich von $`-1 < a_1 < 1`$ herauskommen. 

[zurück zu C1](#C1)


### Aufgabe 2


<a name="L21">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.1:
</a>

1. Definieren Sie sich zunächst Ihre beiden Arrays mit `x = np.array([....])`
2. Berechnen Sie mit `np.convolve` den Ausgang
3. Plotten Sie das Ergebnis. Nachrechnen hilft zwar auch, aber die Faltung ist schon richtig in numpy.

[zurück zu C2](#C2)

<a name="L22">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.2:
</a>


1. Da Sie beim plot-Befehl auch immer die x-Achsen Elemente mitangeben können, brauchen Sie also einen Vektor der die Zeitindices beinhaltet. Diesen können Sie als Liste mit `range` bauen und anschließend in ein numpy Array umwandeln `np.array(ArrayInListe)`. 
2. Überlegen Sie sich, wie lang ihre Vektoren sein müssen, um alle negativen und positiven Zeitindices abzudecken. Erweitern Sie diese um zusätzliche 2 Zeitschritte.
3. Plotten Sie als Test x und h auf dieser ganzen Zeitbasis als stem-Folgen. Sie sollten also für die negativen Zeitindices Nullen sehen. Bei Null ihren ersten echten Wert usw.

[zurück zu C2](#C2)

<a name="L23">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.3:
</a>

1. Eine Möglichkeit ist: Der Film zeigt zunächst die zeitliche Umkehrung von z.B. h. Evtl Texteinblendung. Anschließend wird die zeitlich gespiegelte Folge über die andere Folge geschoben und das Ergebnis in einem zweiten subplot darunter immer gezeigt. Das weiterschieben erfolgt immer nach ca. 1s (Kann zu schnell oder langsam sein. Hier geht es nur darum sich zunächst ein Konzept zu überlegen).
2. Die Ausgangsfolge muss 4 Werte lang sein. Wenn wir noch 2 Nullmulitplikationen (da wo nichts passiert) vorne und hinten anhängen, folgt daraus, dass wir Vektoren der Länge 10 (4 pos, 2 neg + 4 null) brauchen und insgesamt 10 Einzelplots plus des Eingangsplot mit der zeitlichen Spiegelung. Diese sollte vielleicht etwas länger sichtbar sein.
3. Erzeugen Sie die einzelnen Plots (siehe nächste Hilfestellungen). Sie können es sich hier einfach machen und die Zwischenergebnisse als einzelne Vektoren oder als Matrix speichern. Hier geht es nicht darum möglichst flexibel zu sein, sondern didaktisch. Natürlich ist es noch besser, wenn anschließend x und h beliebig sein könnten. 

[zurück zu C2](#C2)


<a name="L24">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.4:
</a>

1. Eine zeitliche Spiegelung kann durch Indexoperatoren mit einer Schrittweite von -1 erreicht werden. Bsp:
```python
a = np.array([1,2,3,4])

print(a)
print(a[::-1])
```
2. Plotten Sie die zeitlich gespiegelte Folge (diese ist dann nicht-kausal) mit dem passenden Zeitvektor (nicht-kausal)
3. Wiederholen Sie das plotten in einer neuen Figure mit zwei untereinander liegenden subplots (hier oben), aber diesmal mit vorne und hinten angefügten Nullen, um unsere Gesamtlänge von 8 zu erreichen. 

[zurück zu C2](#C2)

<a name="L25">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.5:
</a>

1. Plotten Sie nun die andere kausale Folge in einer anderen Farbe, mit Nullen angefügt und dem gleichen Zeitvektor wie oben. 
2. Vergessen Sie nicht, dass für diese Art der Folgendarstellung `stem` besser geeignet ist als `plot`.

[zurück zu C2](#C2)

<a name="L26">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.6:
</a>

1. Sie verschieben anschließend die Folge immer um einen Zeitindex weiter. 
2. Erzeugen Sie eine neue Figure und plotten die um einen Takt verschobene Folge und die zweite Folge
3. Wiederholen Sie dies für alle Takte bis die zeitlich gespiegelte Folge einmal durchgelaufen ist.
4. Zeichnen Sie anschließend in alle Bilder in dem zweiten subplot das Ergebnis der Faltung. Es kommt also immer nur ein neuer Wert hinzu.

[zurück zu C2](#C2)

<a name="L27">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.7:
</a>


1. Dies ist erneut etwas komplexer. Sie finden unter https://stackoverflow.com/questions/11874767/how-do-i-plot-in-real-time-in-a-while-loop-using-matplotlib einige Möglichkeiten.
2. Die Lösung mit der for-Schleife is die einfachste. 
3. Überlegen Sie sich, wie die einzelnen subplots aufgebaut werden müssen, damit Sie von einer Zählvariable abhängig sind
4. Falls Sie Ihr Ergebnis als Video abspeichern wollen, müssen Sie über andere Lösungen nachdenken. `animation` in `matplotlib` bietet einige Möglichkeiten Bilder (figures) in einer Animation einzubauen. Eine Möglichkeit ist `matplotlib.animation.FuncAnimation` zu nutzen. 

[zurück zu C2](#C2)



